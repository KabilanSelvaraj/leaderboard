import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })

export class FitpayService {

  constructor(private http: HttpClient) { }

  getLeaderBoard(date: string) {

    // var contestants = this.http.get('../assets/contestants.json');

    // var contestants = this.http.get('https://run.mocky.io/v3/7b2205a7-ac27-42c5-ab10-cca899637802');

    var contestants = this.http.post(
      `https://dev.fitpay.in/graphql`, { 
        query: `query getLeaderBoard($date:String) { getRankingByDate(date:$date) { userId displayName totalSteps userProfilePicUrl } }`, 
        variables: `{ "date": "${date}" }` 
      }
    );

    return contestants;
  }

}
