import { Component, OnInit, ViewChild } from '@angular/core';
import { FitpayService } from 'src/app/fitpay.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'contestants',
  templateUrl: './contestants.component.html',
  styleUrls: ['./contestants.component.css']
})

export class ContestantsComponent implements OnInit {

  constructor(private fitpayService: FitpayService) { }

  displayedColumns: string[] = ['no', 'userProfilePicUrl', 'userId', 'displayName', 'totalSteps'];

  dataSource: any;

  date: any;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  
  ngOnInit(): void {
    this.date = new Date();
    this.getLeaderBoard(this.date);
  }

  getLeaderBoard(date: Date) {
    var dateString = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    this.fitpayService.getLeaderBoard(dateString).subscribe(response => {
      this.dataSource = response;
      this.dataSource = new MatTableDataSource(this.dataSource.data.getRankingByDate);
      this.dataSource.paginator = this.paginator;
    });
  }

  searchData(event: any) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
