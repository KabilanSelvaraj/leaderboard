import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeaderboardComponent } from './leaderboard.component';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ContestantsComponent } from './contestants/contestants.component';
import { PrizeComponent } from './prize/prize.component';
import { FormsModule } from '@angular/forms';

const materials = [
  MatButtonModule,
  MatTableModule,
  MatCardModule,
  MatGridListModule,
  MatFormFieldModule,
  MatInputModule,
  MatPaginatorModule, 
  MatDatepickerModule
];

@NgModule({
  declarations: [ 
    LeaderboardComponent, 
    ContestantsComponent, 
    PrizeComponent 
  ],
  imports: [ 
    CommonModule,
    FormsModule,
    materials 
  ],
  exports: [ 
    LeaderboardComponent 
  ]
})

export class LeaderboardModule { }
