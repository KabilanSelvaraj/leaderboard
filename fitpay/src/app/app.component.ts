import { Component, OnInit } from '@angular/core';
import { FitpayService } from './fitpay.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'fitpay';
  RippleColor = "rgb(40,40,40)";
}
