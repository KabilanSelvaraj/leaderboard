import { TestBed } from '@angular/core/testing';

import { FitpayService } from './fitpay.service';

describe('FitpayService', () => {
  let service: FitpayService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FitpayService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
